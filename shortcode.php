<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Sagaio_Recently_Purchased_Products_Shortcode {

	static $add_script;
	var $wpvepdf_attr = false;

	static function init() {

		add_filter('widget_text', 'do_shortcode');
		add_shortcode('sagaio_rpps', array(new Sagaio_Recently_Purchased_Products_Shortcode(), 'sagaio_rpps_shortcode'));
		add_action('wp_enqueue_scripts', array(__CLASS__, 'register_script_style'));
		add_action('wp_enqueue_scripts', array(__CLASS__, 'print_style'));

	}

	public function sagaio_rpps_shortcode($attr) {

		$num_product  = (isset($attr['num_product']))?(int)$attr['num_product']:4;
		$theme      = (isset($attr['theme']))?$attr['theme']:'theme-list-view';


		switch($theme){

			case 'theme-list-view':
				$html     = sagaio_rpps_theme_list_view($num_product);
			break;
			case 'theme-hover':
				$html 	= sagaio_rpps_theme_hover($num_product);
			break;
			default:
				$html     = sagaio_rpps_theme_list_view($num_product);
			break;

		}

		return $html;

	}

	static function register_script_style() {
		wp_register_style('sagaio-rpps',  CE_CDXWOOFREEREP_PLUGIN_URL. '/assets/css/sagaio-rpps.css');
	}

	static function print_style(){

		wp_enqueue_style('cdxrep-front');
	}

}

Sagaio_Recently_Purchased_Products_Shortcode::init();
?>