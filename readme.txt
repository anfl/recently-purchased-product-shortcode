=== Recently Purchased Products Shortcode ===
Contributors: sagaio
Tags: woocommerce, woocommerce plugin, widgets, recently purchased, woocommerce recently purchased, woocommerce recently purchased product, recently purchased widget, recently purchased shortcode, recently purchased product widget, recently purchased product display
Requires at least: 3.8
Tested up to: 4.7
WC requires at least: 2.3
WC tested up to: 2.6.11
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Recently Purchased Products Shortcode using widget and shortcode to boost your woocommerce products sells.

== Description ==

Based on Recently Purchased Product Display For WooCommerce by codextent.

Recently Purchased Products Display For WooCommerce Plugin is a WordPress WooCommerce add-on plugin that display your product up sells products using widgets and shortcode.

### OVERVIEW
WooCommerce Recently Purchased Products Display Plugin provide you the option to display your woocommerce products from past completed orders using easy to use widgets and shortcodes.

### How it works?
1. After successfull plugin activation go to your wordpress admin `Widgets` page.
2. If you are in admin `Widgets` page, you can find a widget named **Recently Purchased Products Shortcode**.
3. Drag this **Recently Purchased Products Shortcode** widget inside your desired sidebar then fill the widget fields and save.
4. Now check the front end.
5. You can also use shortcode to display Recently Purchased Products. For this use the shortcode in any text widget, in post content, in page content or using php code <?php echo do_shortcode('[sagaio_rpps pid="101" theme="theme-list-view"]'); ?>.

### SHORTCODE
You can also use [sagaio_rpps] shortcode to display woocommerce recently purchased products.
This shortcode has two option/attribute
**1. num_product** - It denotes to total number of prodcuts you want to show in front end display.
**2. theme** - It denotes to display layout. Use "theme-list-view" or "theme-hover" as its value.

### SHORTCODE Example
[sagaio_rpps pid="101" theme="theme-list-view"]

### IMPORTANT NOTICE

- Plugin requires WooCommerce to work.
- You must have atleast one complete order display recently purchased products.

== Installation ==

1. Unzip and upload the `recently-purchased-products-shortcode` directory to the plugin directory (`/wp-content/plugins/`) or install it from `Plugins->Add New->Upload`.
2. Activate the plugin through the `Plugins` menu in WordPress.
3. That's all you need to do. You will notice a new **Woocommerce Up Sells Product Display** widget in your wordpress admin widgets page.

== Frequently Asked Questions ==
= 1. How many layout is included? =
Right now the plugin has 2 display layout included.

= 2. Can we expect more layouts in future? =
Yes we have plan to include more layouts in future versions or update.

== Screenshots ==
1. Admin Woocommerce Recently Purchased Products Display widget
2. Front view of the Recently Purchased Products Display For Woocommerce plugin

== Changelog ==

= 0.1: 2017-01-10 =
* Initial remake of original.